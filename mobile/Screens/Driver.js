import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableOpacity,
    ActivityIndicator,
    TouchableHighlight,
    Alert
} from 'react-native';
import SocketIO from "socket.io-client"
import _ from "lodash";
import PolyLine from "@mapbox/polyline";
import MapView, {Marker, Polyline} from "react-native-maps";


const serverAddress = {
    ip: 'http://YOUR_LOCAL_IP_ADDRESS',
    port: 'YOUR_SOCKET_PORT'
};

export default class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            error: "",
            latitude: -6.300527,
            longitude: 106.639861,
            destination: "",
            predictions:[],
            pointCoords: [],
            routeResponse: null,
            lookingForPassengers: false,

        };
    }

    lookForPassenger = async () => {
      this.setState({
          lookingForPassengers: true
      });

      const socket = SocketIO.connect(`${serverAddress.ip}:${serverAddress.port}`);

      socket.on("connect", () => {
        socket.emit("lookingForPassenger");
      });

      socket.on("taxiRequest", routeResponse => {
        Alert.alert("Yay! You get a passenger");
      });
    }
    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState( {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
                // this.getRouteDirections();
            },
            error => this.setState({error: error.message}),
            {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
        )

    }
    async getRouteDirections(placeId, place_id){
        try{
            const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
            const json = await response.json()
            console.log(json)
            const points = PolyLine.decode(json.routes[0].overview_polyline.points)
            const pointCoords = points.map(point =>
                {
                    return{latitude:point[0],longitude:point[1]}
                }
            )
            this.setState({pointCoords, predictions:[], destination:place_id, routeResponse: json})

            this.map.fitToCoordinates(pointCoords, {
                edgePadding:{top:30, bottom:30, left:30, right:30}
            })
        } catch(error){
            console.error(error)
        }
    }
    async onChangeDestination(destination){
        const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}
                        &radius=2000`
        try{
            const result = await fetch(apiUrl)
            const json = await result.json()
            console.log("iniJson")
            console.log(json)
            this.setState({predictions: json.predictions})
        } catch (e) {
            console.log(e)
        }
    }


    render() {
        let marker = null;
        let findPassengerButton = null;
        let findingPassenger = null;

        findPassengerButton = (
            <View
                style={styles.bottomButton}
            >
                <TouchableOpacity
                    onPress={() => this.lookForPassenger()}
                >
                    <View>
                        <Text style={styles.bottomButtonText}>NARIK MODE</Text>
                    </View>

                </TouchableOpacity>

            </View>



        );
        if (this.state.lookingForPassengers===true){
            findPassengerButton = null;
            findingPassenger = (
                <View
                    style={styles.bottomButton}
                >
                    <Text style={[styles.bottomButtonText, {marginBottom: 20}]}>
                        FINDING PASSENGER
                    </Text>
                    <ActivityIndicator
                        size="large"
                        color="#fff"
                    />

                </View>
            );
        }


        if (this.state.pointCoords.length > 1) {
            marker = (
                <Marker
                    coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]}
                />
            );
        }
        const predictions = this.state.predictions.map(prediction => (
            <TouchableHighlight
                key={prediction.id}
                onPress={() => this.getRouteDirections(prediction.place_id, prediction.description)}
            >
                <View>
                    <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
                </View>
            </TouchableHighlight>
        ));

        return (
            <View style={styles.container}>
                <MapView
                    ref={ map =>
                        this.map = map

                    }

                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    showsUserLocation={true}
                >
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2.5}
                        strokeColor={"red"}
                    />
                    {marker}

                </MapView>
                {predictions}
                {findingPassenger}

                {findPassengerButton}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    bottomButton: {
        backgroundColor: "#010a87",
        marginTop: "auto",
        margin: 20,
        padding: 15,
        paddingHorizontal: 30,
        alignSelf: "center",
        borderRadius: 10,
    },
    bottomButtonText: {
        color: "#fff",
        fontSize: 20,
    },
    suggestions: {
        backgroundColor: '#fff',
        padding: 5,
        fontSize: 18,
        borderWidth: 0.5,
        marginHorizontal: 5,
    },

    destination: {
        padding: 10,
        height: 40,
        borderRadius: 10,
        marginTop: 50,
        marginHorizontal: 10,
        backgroundColor: '#fff'
    },
    container: {
        ...StyleSheet.absoluteFillObject,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});
