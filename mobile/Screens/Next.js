import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button, TouchableOpacity, TouchableHighlight, ActivityIndicator, Image} from 'react-native';
import SocketIO from "socket.io-client"
import MapView, {Polyline, Marker} from "react-native-maps";
import _ from "lodash";

import PolyLine from "@mapbox/polyline";

export default class Next extends Component {

  constructor(props){
    super(props);
    this.state = {
      error: "",
      latitude: -6.300527,
      longitude: 106.639861,
      destination: "",
      predictions:[],
      pointCoords: [],
      routeResponse: null,
      driverButtonPressed: false,
      isNext: true,
      isDriver: false,
      isPassenger: false,

    };
    this.onChangeDestinationDebounced = _.debounce(
      this.onChangeDestination,
      500
    )
  }

  async requestDriver() {
    const socket = SocketIO.connect("http://10.10.103.188:3000");
    socket.on("connect", () => {
      socket.emit("taxiRequest", this.state.routeResponse);
    });
    this.setState({driverButtonPressed:true});
  }
  componentWillMount() {
    navigator.geolocation.getCurrentPosition(
      position => {
        this.setState( {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        })
        // this.getRouteDirections();
      },
      error => this.setState({error: error.message}),
      {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
    )

  }

  async getRouteDirections(placeId, place_id){
    try{
      const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`)
      const json = await response.json()
      console.log(json)
      const points = PolyLine.decode(json.routes[0].overview_polyline.points)
      const pointCoords = points.map(point =>
        {
          return{latitude:point[0],longitude:point[1]}
        }
      )
      this.setState({pointCoords, predictions:[], destination: place_id, routeResponse: place_id})

      this.map.fitToCoordinates(pointCoords, {
        edgePadding:{top:30, bottom:30, left:30, right:30}
      })
    } catch(error){
      console.error(error)
    }
  }

  async onChangeDestination(destination){
    const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw"
    const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}
                        &radius=2000`
    try{
      const result = await fetch(apiUrl)
      const json = await result.json()
      console.log("iniJson")
      console.log(json)
      this.setState({predictions: json.predictions})
    } catch (e) {
      console.log(e)
    }
  }

  render() {
    let marker = null;
    let driverButton = null;
    let loadingIndicator = null;

    if (this.state.pointCoords.length > 1) {
      marker = (
        <Marker
          coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]}
        />
      );

      driverButton = (
        <View
          style={styles.bottomButton}
        >
          <TouchableOpacity
            onPress={() => this.requestDriver()}
          >
            <View>
              <Text style={styles.bottomButtonText}>FIND DRIVER</Text>
            </View>

          </TouchableOpacity>

        </View>

      );
      if (this.state.driverButtonPressed===true) {
        driverButton = null
        loadingIndicator = (
          <View
            style={styles.bottomButton}
          >
            <Text style={[styles.bottomButtonText, {marginBottom: 20}]}>
              Finding Driver
            </Text>
            <ActivityIndicator
              size="large"
              color="#fff"
            />

          </View>

        )
      }

    }



    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        key={prediction.id}
        onPress={() => this.getRouteDirections(prediction.place_id, prediction.description)}
      >
        <View>
          <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
        </View>
      </TouchableHighlight>
    ));

    return (
      <View style={styles.container}>


        <MapView
          ref={ map =>
            this.map = map

          }

          style={styles.map}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: 0.015,
            longitudeDelta: 0.0121,
          }}
          showsUserLocation={true}
        >
          <Polyline
            coordinates={this.state.pointCoords}
            strokeWidth={2.5}
            strokeColor={"red"}
          />
          {marker}

        </MapView>

        <View style={styles.topbar}>
          <TouchableOpacity style={{paddingLeft:15, marginTop: 25}}>
            <Image source={require("./aset/sidebarbutton.png")} style={styles.notification}/>
          </TouchableOpacity>

          <Text style={styles.MyBlueBird}>MyBlueBird</Text>

          <TouchableOpacity style={{marginTop: 25, marginRight: 15, }}>
            <Text style={{fontWeight: 'bold', color: "#FFFFFF"}}>Easy Ride</Text>
          </TouchableOpacity>
        </View>

        <View style={{width: 345, height: 75, backgroundColor: '#0068de', marginLeft: 15,marginRight: 15, marginTop:10, borderRadius: 15}}>
          <View style={{flexDirection: 'row',justifyContent: 'space-between', alignItems: 'center'}}>
            <TouchableOpacity style={{}}>
              <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
              <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{}}>
              <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
              <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{}}>
              <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
              <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
            </TouchableOpacity>

            <TouchableOpacity style={{}}>
              <Image style={{width: 25, height: 25, marginLeft: 20,marginTop: 20}} source={require("./aset/add.png")}/>
              <Text style={{fontSize: 11, marginTop: 5, marginRight: 5}}> Add favorite</Text>
            </TouchableOpacity>
          </View>
        </View>



        <View style={{width: 345, height: 55, flexDirection: 'row',margin: 5, justifyContent: 'center',marginLeft: 15}}>

          <TouchableOpacity style={{width: 165, height: 55, flexDirection: 'column', backgroundColor: "#d6d6d6", margin: 5, justifyContent: 'center', borderRadius: 10, borderColor: "#999999",borderWidth: 3}}>
            <Text style={{fontWeight: 'bold', fontSize: 12, color: '#0068de', marginLeft: 13}}>PICK-UP</Text>
            <Text style={{color: "#808080", fontSize: 12, fontWeight: 'bold', marginLeft: 10, marginTop: 10}}>Ellis St</Text>

          </TouchableOpacity>


          <TouchableOpacity style={{width: 165, height: 55, flexDirection: 'column', backgroundColor: "#FFFFFF", margin: 5, justifyContent: 'center', borderRadius: 10, borderColor: "#5eacd7",borderWidth: 3}}>
            <Text style={{fontWeight: 'bold', fontSize: 12, marginLeft: 10, color: '#808080'}}>DROP-OFF</Text>
            <TextInput
              placeholder={"Set Location"}
              style={styles.desto}
              value={this.state.destination}
              placeholderTextColor={"#686868"}
              onChangeText={destination => {
                this.setState({destination});
                this.onChangeDestinationDebounced(destination)
              }}
            >
            </TextInput>
          </TouchableOpacity>


        </View>
        {predictions}

        <TextInput
          placeholder={"Add pickup instructions for driver."}
          style={styles.destination}
          placeholderTextColor={"#686868"}
        >
        </TextInput>

        <View style={{width: 375, height: 180, flexDirection: 'column', backgroundColor: "#f4f4f4", marginTop: 12}}>
          <View style={{flexDirection: 'row',width:375}}>
            <Image style={{width: 85, height: 50, marginLeft: 15}} source={require("./aset/texi.png")}/>
            <Text style={{width:75, color: "#000000", margin: 20, fontSize: 12, fontWeight: 'bold'}}>Blue Bird</Text>

           <View style={{flexDirection: 'column', marginTop: 10}}>
            <Text style={{width:150, color: "#999999",fontSize: 11,textAlign: 'right', fontWeight: 'bold'}}>ESTIMATED COST</Text>
            <Text style={{width:150, color: "#000000",fontWeight: 'bold', fontSize: 11,textAlign: 'right'}}>Choose destination first</Text>
           </View>
          </View>


          <View style={{width: 375, height: 50, flexDirection: 'row',marginTop: 5, justifyContent: 'center'}}>

            <TouchableOpacity style={{width: 175, height: 50, flexDirection: 'row', backgroundColor: "#FFFFFF", margin: 5, borderRadius: 5, borderColor: "#808080", alignItems: 'center', borderBottomColor: "#d6d6d6", borderBottomWidth: 3}}>
              <Image style={{width: 12, height: 12, marginLeft: 15}} source={require("./aset/lockicon.png")}/>
              <Text style={{fontSize: 14, marginLeft: 15, color: "#808080", fontWeight: 'bold'}}>Now</Text>
              <Image style={{width: 12, height: 12, marginLeft: 80, marginTop: 1}} source={require("./aset/arrow.png")}/>
            </TouchableOpacity>


            <TouchableOpacity style={{width: 175, height: 50, flexDirection: 'row', backgroundColor: "#FFFFFF", margin: 5, borderRadius: 5, borderColor: "#808080", alignItems: 'center', borderBottomColor: "#d6d6d6", borderBottomWidth: 3}}>
              <Image style={{width: 12, height: 12, marginLeft: 15}} source={require("./aset/lockicon.png")}/>
              <Text style={{fontSize: 14, marginLeft: 15, color: "#d2d2d2", fontWeight: 'bold'}}>Trip purpose</Text>
              <Image style={{width: 12, height: 12, marginLeft: 25, marginTop: 1}} source={require("./aset/arrow.png")}/>
            </TouchableOpacity>

          </View>

          <View style={{width: 375, height: 50, flexDirection: 'row',marginTop: 5, justifyContent: 'center'}}>

            <TouchableOpacity style={{width: 175, height: 50, flexDirection: 'row', backgroundColor: "#FFFFFF", margin: 5, borderRadius: 5, borderColor: "#808080", alignItems: 'center', borderBottomColor: "#d6d6d6", borderBottomWidth: 3}}>
              <Image style={{width: 12, height: 12, marginLeft: 15}} source={require("./aset/lockicon.png")}/>
              <Text style={{fontSize: 14, marginLeft: 15, color: "#808080", fontWeight: 'bold'}}>Cash</Text>
              <Image style={{width: 12, height: 12, marginLeft: 76, marginTop: 1}} source={require("./aset/arrow.png")}/>
            </TouchableOpacity>


            <TouchableOpacity style={{width: 175, height: 50, flexDirection: 'row', backgroundColor: "#ebebeb", margin: 5, borderRadius: 5, borderColor: "#808080", alignItems: 'center', borderBottomColor: "#d6d6d6", borderBottomWidth: 3}}>
              <Image style={{width: 12, height: 12, marginLeft: 15}} source={require("./aset/lockicon.png")}/>
              <Text style={{fontSize: 14, marginLeft: 15, color: "#d2d2d2", fontWeight: 'bold'}}>For epay only</Text>
              <Image style={{width: 12, height: 12, marginLeft: 20, marginTop: 1}} source={require("./aset/arrow.png")}/>
            </TouchableOpacity>

          </View>

        </View>

        <View style={{justifyContent: 'flex-end'}}>
          <View style={styles.bottombar}>
            <TouchableOpacity style={{ width: 375, height: 55,alignItems: 'center',justifyContent: 'center'}}>
              <Text style={{color: "#FFFFFF", fontWeight: 'bold'}}>BOOK BLUE BIRD NOW</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bottomButton: {
    backgroundColor: "#010a87",
    marginTop: "auto",
    margin: 20,
    padding: 15,
    paddingHorizontal: 30,
    alignSelf: "center",
    borderRadius: 10,
  },
  bottomButtonText: {
    color: "#fff",
    fontSize: 20,
  },
  suggestions: {
    backgroundColor: '#fff',
    padding: 5,
    fontSize: 18,
    borderWidth: 0.5,
    marginHorizontal: 5,
  },

  destination: {
    padding: 10,
    height: 45,
    borderRadius: 7,
    marginHorizontal: 10,
    marginTop: 300,
    backgroundColor: '#fff',
    borderColor: "#999999",
    borderWidth: 2
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'column',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  topbar: {
    width: 375,
    height: 75,
    flexDirection: 'row',
    backgroundColor: '#0068de',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

    MyBlueBird: {
      fontWeight: 'bold',
      fontSize: 24,
      marginTop: 25,
      marginLeft: 35,
      color: '#FFFFFF'
    },
    notification: {
      height: 25,
      width: 25,
  },
  bottombar: {
    width: 375,
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#0068de',
    justifyContent: 'space-between',
  },
  desto: {
    padding: 1
  },
});
