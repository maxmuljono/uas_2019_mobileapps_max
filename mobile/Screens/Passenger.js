import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput, Button, TouchableOpacity, TouchableHighlight, ActivityIndicator, Image} from 'react-native';
import SocketIO from "socket.io-client"
import MapView, {Polyline, Marker} from "react-native-maps";
import _ from "lodash";
import PolyLine from "@mapbox/polyline";
import Next from "./Next";


export default class Passenger extends Component {

    constructor(props){
        super(props);
        this.state = {
            error: "",
            latitude: -6.300527,
            longitude: 106.639861,
            destination: "",
            predictions:[],
            pointCoords: [],
            routeResponse: null,
            driverButtonPressed: false,
            isDriver: false,
            isNext: false,
            isPassenger: true,

        };
        this.onChangeDestinationDebounced = _.debounce(
            this.onChangeDestination,
            500
        )
    }

    async requestDriver() {
        const socket = SocketIO.connect("http://10.10.103.188:3000");
        socket.on("connect", () => {
            socket.emit("taxiRequest", this.state.routeResponse);
        });
        this.setState({driverButtonPressed:true});
    }
    componentWillMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState( {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                })
                // this.getRouteDirections();
            },
            error => this.setState({error: error.message}),
            {enableHighAccuracy: true, maximumAge: 2000, timeout: 20000}
        )

    }

    async getRouteDirections(placeId, place_id){
        try{
            const response = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${this.state.latitude},${this.state.longitude}&destination=place_id:${placeId}&key=AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw`);
            const json = await response.json();
            console.log(json);
            const points = PolyLine.decode(json.routes[0].overview_polyline.points);
            const pointCoords = points.map(point =>
                {
                    return{latitude:point[0],longitude:point[1]}
                }
            );
            this.setState({pointCoords, predictions:[], destination: place_id, routeResponse: place_id});

            this.map.fitToCoordinates(pointCoords, {
                edgePadding:{top:30, bottom:30, left:30, right:30}
            })
        } catch(error){
            console.error(error)
        }
    }

    async onChangeDestination(destination){
        const apikey = "AIzaSyCvNbQ9GEZ3p3-U2yamS8UrfHmD_e7Iqmw";
        const apiUrl = `https://maps.googleapis.com/maps/api/place/autocomplete/json?key=${apikey}
                        &input=${destination}&location=${this.state.latitude}, ${this.state.longitude}
                        &radius=2000`;
        try{
            const result = await fetch(apiUrl);
            const json = await result.json();
            console.log("iniJson");
            console.log(json);
            this.setState({predictions: json.predictions})
        } catch (e) {
            console.log(e)
        }
    }

    render() {
        let marker = null;
        let driverButton = null;
        let loadingIndicator = null;
      if (this.state.isNext) {
        return <Next/>
      }
        if (this.state.pointCoords.length > 1) {
            marker = (
                <Marker
                    coordinate={this.state.pointCoords[this.state.pointCoords.length - 1]}
                />
            );

            driverButton = (
                <View
                    style={styles.bottomButton}
                >
                    <TouchableOpacity
                        onPress={() => this.requestDriver()}
                    >
                        <View>
                            <Text style={styles.bottomButtonText}>FIND DRIVER</Text>
                        </View>

                    </TouchableOpacity>

                </View>

            );
            if (this.state.driverButtonPressed===true) {
                driverButton = null;
                loadingIndicator = (
                    <View
                        style={styles.bottomButton}
                    >
                        <Text style={[styles.bottomButtonText, {marginBottom: 20}]}>
                            Finding Driver
                        </Text>
                        <ActivityIndicator
                            size="large"
                            color="#fff"
                        />

                    </View>

                )
            }

        }



        const predictions = this.state.predictions.map(prediction => (
            <TouchableHighlight
                key={prediction.id}
                onPress={() => this.getRouteDirections(prediction.place_id, prediction.description)}
            >
                <View>
                    <Text style={styles.suggestions} key={prediction.id}>{prediction.description}</Text>
                </View>
            </TouchableHighlight>
        ));

        return (
            <View style={styles.container}>


                <MapView
                    ref={ map =>
                        this.map = map

                    }

                    style={styles.map}
                    region={{
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                        latitudeDelta: 0.015,
                        longitudeDelta: 0.0121,
                    }}
                    showsUserLocation={true}
                >
                    <Polyline
                        coordinates={this.state.pointCoords}
                        strokeWidth={2.5}
                        strokeColor={"red"}
                    />
                    {marker}

                </MapView>






              <View style={styles.topbar}>
                <TouchableOpacity style={{paddingLeft:15, marginTop: 25}}>
                  <Image source={require("./aset/sidebarbutton.png")} style={styles.notification}/>
                </TouchableOpacity>

                  <Text style={styles.MyBlueBird}>MyBlueBird</Text>

                <TouchableOpacity style={{marginTop: 25, marginRight: 15, }}>
                  <Text style={{fontWeight: 'bold', color: "#FFFFFF"}}>Easy Ride</Text>
                </TouchableOpacity>
              </View>

              <View style={{width: 345, height: 75, backgroundColor: '#0068de', marginLeft: 15,marginRight: 15, marginTop:10, borderRadius: 15}}>
                <View style={{flexDirection: 'row',justifyContent: 'space-between', alignItems: 'center'}}>
                  <TouchableOpacity style={{}}>
                    <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
                    <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{}}>
                    <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
                    <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{}}>
                    <Image style={{width: 25, height: 25, marginLeft: 25,marginTop: 20}} source={require("./aset/add.png")}/>
                    <Text style={{fontSize: 11, marginLeft: 5, marginTop: 5}}> Add favorite</Text>
                  </TouchableOpacity>

                  <TouchableOpacity style={{}}>
                    <Image style={{width: 25, height: 25, marginLeft: 20,marginTop: 20}} source={require("./aset/add.png")}/>
                    <Text style={{fontSize: 11, marginTop: 5, marginRight: 5}}> Add favorite</Text>
                  </TouchableOpacity>
                </View>
              </View>

                <TextInput
                    placeholder={"Set your destination"}
                    style={styles.destination}
                    value={this.state.destination}
                    placeholderTextColor={"#686868"}
                    onChangeText={destination => {
                      this.setState({destination});
                      this.onChangeDestinationDebounced(destination)
                    }}
                >
                </TextInput>
              {predictions}
              <TouchableOpacity style={{backgroundColor: "#FFFFFF", width: 35, height: 35, alignItems: 'center', justifyContent: 'center', marginTop: 515, marginLeft: 325}}>
                <Image style={{width: 25, height: 25}} source={require("./aset/gps.png")}/>
              </TouchableOpacity>

              <View style={{justifyContent: 'flex-end', marginTop: 10}}>
                <View style={styles.bottombar}>
                  <TouchableOpacity style={{width:185}}>
                    <Image style={{width: 155, height: 50}} source={require("./aset/texi.png")}/>
                  </TouchableOpacity>

                  <TouchableOpacity onPress={() => this.setState({ isNext: true})} style={{ width: 175, height: 50,alignItems: 'center',marginTop:15, marginRight: 15}}>
                    <Text style={{color: "#FFFFFF", fontWeight: 'bold'}}>Next</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bottomButton: {
        backgroundColor: "#010a87",
        marginTop: "auto",
        margin: 20,
        padding: 15,
        paddingHorizontal: 30,
        alignSelf: "center",
        borderRadius: 10,
    },
    bottomButtonText: {
        color: "#fff",
        fontSize: 20,
    },
    suggestions: {
        backgroundColor: '#fff',
        padding: 5,
        fontSize: 18,
        borderWidth: 0.5,
        marginHorizontal: 5,
    },

    destination: {
        padding: 5,
        height: 40,
        borderRadius: 10,
        marginHorizontal: 10,
        marginTop: 5,
        backgroundColor: '#fff',
        borderWidth: 3,
        borderColor: "#5eacd7",
    },
    container: {
        ...StyleSheet.absoluteFillObject,
      flexDirection: 'column',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
    topbar: {
      width: 375,
      height: 75,
      flexDirection: 'row',
      backgroundColor: '#0068de',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    MyBlueBird: {
      fontWeight: 'bold',
      fontSize: 24,
      marginTop: 25,
      marginLeft: 35,
      color: '#FFFFFF'
    },
    notification: {
      height: 25,
      width: 25,
    },
  bottombar: {
    width: 375,
    height: 50,
    flexDirection: 'row',
    backgroundColor: '#0068de',
    justifyContent: 'space-between',
  },
  predictions: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#fff'
  },
});
