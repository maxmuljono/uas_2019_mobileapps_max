/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TextInput} from 'react-native';
import io from "socket.io-client"

export default class Socket extends Component {
    constructor(props){
        super(props);
        this.state = {
            chatMessage:"",
            chatMessages: [],
        }
    }
    componentWillMount() {
        this.socket = io("http://10.10.103.94:3000");
        this.socket.on("chat message", msg => {
            this.setState({chatMessages: [...this.state.chatMessages, msg]})
        })
    }

    submitChatMessage() {
        this.socket.emit("chat message", this.state.chatMessage);
        this.setState({chatMessage:""})
    }

    render() {
        const chatMessages = this.state.chatMessages.map(chatMessage => (
            <Text key ={chatMessage}>{chatMessage}</Text>
        ))
        return (
            <View style={styles.container}>
                <TextInput
                    style={{width: "90%", borderWidth: 2}}
                    autoCorrect={false}
                    value={this.state.chatMessage}
                    onSubmitEditing={() => this.submitChatMessage()}
                    onChangeText={chatMessage => {
                      this.setState({chatMessage});
                    }}
                />
                {chatMessages}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
